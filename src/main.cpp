#include <pcl/console/parse.h>
 #include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <iostream>
#include <time.h> 
#include "opencv2/highgui/highgui.hpp"

typedef pcl::PointXYZRGBA PointT;																																																																															

int main (int argc, char ** argv)
{

  std::cout << "This program simply constructs the rgb image of the point cloud given as input\n";

  pcl::PointCloud<PointT> cloud;
  pcl::console::print_highlight ("Loading point cloud...\n");
  if (pcl::io::loadPCDFile<PointT> (argv[1], cloud))
  {
    pcl::console::print_error ("Error loading cloud file!\n");
    return (1);
  }
  std::cout << "pcd loaded: size: "<< cloud.size() <<"\n";

  cv::Mat img(480, 640, CV_8UC3); //create an image ( 3 channels, 8 bit image depth);

  for (int row = 0; row < img.rows; ++row)
  {
  	for (int c = 0; c < img.cols; ++c)
  	{							
  		
  		pcl::PointXYZRGBA point = cloud(c,row); //note: there is a transformation in the reference frame of the pclò and the image!!!!!!!!!!!!!!!!

  		uint8_t r = (uint8_t)point.r;
  		uint8_t g = (uint8_t)point.g;	 																																																																																																																																																																																																																																																																																																																																																																																																																																																																																										
  		uint8_t b = (uint8_t)point.b;	 																																																																																																																																																																																																																																																																																																																																																																																																																																																																																										
	 							

  		cv::Vec3b color;// = cloud(r,c);	//vector of colors						 
  		color.val[0] = r;
  		color.val[1] = g;
  		color.val[2] = b;
  		img.at<cv::Vec3b>(row,c) = color;
  	}
  }

  cv::namedWindow("MyWindow", CV_WINDOW_AUTOSIZE); //create a window with the name "MyWindow"
  cv::imshow("MyWindow", img); //display the image which is stored in the 'img' in the "MyWindow" window

  cv::waitKey(0);

  return 0;
}